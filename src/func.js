const getSum = (str1, str2) => {
    if (typeof str1 !== 'string' || typeof str2 !== 'string') {
        return false;
    }

    const val1 = +str1;
    const val2 = +str2;

    if (isNaN(val1) || isNaN(val2)) {
        return false;
    }

    return val1 + val2 + '';
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
    let posts = 0, comments = 0;

    for (const post of listOfPosts) {
        if (post.author === authorName) {
            posts++;
        }

        if (post.comments !== undefined) {
            for (const comment of post.comments) {
                if (comment.author === authorName) {
                    comments++;
                }
            }
        }
    }

    return `Post:${posts},comments:${comments}`;
};

const tickets = (people) => {
    let money = [];

    for (const bill of people) {
        let change = bill - 25;

        let i = 0;
        while (i < money.length) {
            if (change >= money[i]) {
                change -= money[i];
                money.splice(i, 1);
            } else i++;
        }

        if (change > 0) return 'NO';

        money.push(bill);
        money.sort((a, b) => b - a);
    }

    return 'YES';
};

module.exports = {getSum, getQuantityPostsByAuthor, tickets};
